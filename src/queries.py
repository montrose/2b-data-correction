from typing import List
from datetime import datetime


# data transfer object, less params in functions
class EventThresholds:
    def __init__(self, seconds_span: int, stable_period: int, low: float, high: float):
        self.seconds_span = seconds_span
        self.stable_period = stable_period
        self.low = low
        self.high = high


class DatumRepository:
    def __init__(self, conn, max_days_range: int = 60, downtime: int = 360):
        self.conn = conn
        self.cursor = conn.cursor()
        self.MAX_DAYS_RANGE = max_days_range
        self.downtime = downtime

    # Returns all rows for given param and dates
    def get_by_range(self, param_id: int, start: datetime, end: datetime):
        if start >= end:
            raise ValueError("Start date must be less than end date")

        days_range = (end - start).days
        if days_range > self.MAX_DAYS_RANGE:
            raise ValueError(
                f"Day span between start and end exceeds max range of {self.MAX_DAYS_RANGE}"
            )

        query = f"""
            select DataId, ParameterId, PostDate, Invalidate, NumericValue from dbo.Datum
            where ParameterId = {param_id} and
            PostDate > N'{start.strftime('%Y-%m-%d %H:%M:%S')}' and
            Postdate <=  N'{end.strftime('%Y-%m-%d %H:%M:%S')}'
            order by PostDate asc"""

        self.cursor.execute(query)

        return self.cursor.fetchall()

    def get_inst_and_org(self, param_id: int):
        query = f"""
            Select I.InstrumentId, S.OrganizationId
            from dbo.Parameters P
            join dbo.Instruments I on p.InstrumentId = I.InstrumentId
            join dbo.sites S on S.siteId = I.SiteId
            where P.ParameterId = {param_id}"""

        self.cursor.execute(query)

        return self.cursor.fetchall()

    # Identifies periods of suspect data based on downtime
    def find_events(self, data) -> List[int]:
        if len(data) < 2:
            raise IndexError("Data does not contain enough points")

        event_ids = []

        for i in range(1, len(data[1:])):
            prev = data[i - 1]
            curr = data[i]
            previousSec = prev[2]
            currentSec = curr[2]
            seconds_diff = (currentSec - previousSec).total_seconds()

            if (seconds_diff >= self.downtime and curr[1] == prev[1]):  # Seconds more than threhold AND paramIds Match
                event_ids.append(i)

        return event_ids

    def invalidate_events(self, param_id: int, start_date: datetime, end_date: datetime, thresholds: EventThresholds) -> bool:
        data = self.get_by_range(param_id, start_date, end_date)
        count = len(data)

        if count < 2:
            return False

        event_ids = self.find_events(data)

        for idx in event_ids:
            reads = []  # A running log of all the rows in an event
            start = data[idx]
            reads.append(start)

            eventMax = 0  # to record the highest value in a given event
            k = 1
            startPostDate = start[2]
            endPostDate = start[2]  # if only sole points
            while True:
                if (idx + k) >= count:
                    break

                nextRow = data[idx + k]
                time_diff = (nextRow[2] - start[2]).total_seconds()
                # Stop if we exceed the search-time or if the parameter changes
                if (time_diff >= thresholds.seconds_span or nextRow[1] != start[1]):
                    break

                # Stop if after a set amount of time readings are under a certain amount
                if (time_diff >= thresholds.stable_period and nextRow[-1] < thresholds.low):
                    break

                endPostDate = nextRow[2]
                reads.append(nextRow)
                k += 1

            eventMax = max([float(d[-1]) for d in reads])

            # We only want to invalidate if there was a time where values were ever above a high threshold now
            if eventMax >= thresholds.high and reads[0][-2] is False:
                for point in reads:
                    self.cursor.execute(
                        "UPDATE dbo.Datum set Invalidate =  N'1' where DataId = %d",
                        int(point[0]),
                    )
                quickQuery = self.get_inst_and_org(param_id)
                instrumentId = quickQuery[0][0]
                organizationId = quickQuery[0][1]

                longQuery = f"""INSERT into dbo.DataTag values(
                        {instrumentId},
                        {point[1]},
                        N'{startPostDate.strftime('%Y-%m-%d %H:%M:%S')}',
                        N'{endPostDate.strftime('%Y-%m-%d %H:%M:%S')}',
                        {k},
                        'Spike Detection',
                        N'1',
                        'via Script',
                        N'{datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')}',
                        0,
                        {organizationId})
                       """
                self.cursor.execute(longQuery)
                self.conn.commit()  # invalidate all of the rows in reading

        return True
