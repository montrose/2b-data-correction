from queries import EventThresholds

# seconds_span of events after downtime, stablization_period, lowBaseline, highBaseline
PARAMS = {
    681: EventThresholds(180, 60, 12.0, 24.0),

    693: EventThresholds(180, 60, 12.0, 24.0),

    2829: EventThresholds(180, 60, 12.0, 24.0),

    12933: EventThresholds(180, 60, 12.0, 24.0),

    13725: EventThresholds(180, 60, 12.0, 24.0)
}
