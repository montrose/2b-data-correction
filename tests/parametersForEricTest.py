from queries import EventThresholds

#seconds_span of events after downtime, stablization_period, lowBaseline, highBaseline
PARAMS = {
    1: EventThresholds(120, 60, 12.0, 24.0),

    2: EventThresholds(120, 60, 12.0, 24.0),

    3: EventThresholds(120, 60, 12.0, 24.0),

    4: EventThresholds(120, 60, 12.0, 24.0),

    5: EventThresholds(120, 60, 12.0, 24.0)
}
