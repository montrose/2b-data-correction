import os
import traceback
import pymssql
from datetime import datetime, timedelta
from time import sleep

from queries import DatumRepository
from parameters import PARAMS

# pass options through env variables
MAX_DAY_RANGE = 30
SLEEP = int(os.environ.get("SLEEP", "21600"))  # 21600 = 6 hours
BACKFILL = os.environ.get("BACKFILL",True)
SERVER = os.environ.get("SERVER", "127.0.0.1")
USER = os.environ.get("USER","SA")
PASSWORD = os.environ.get("PASSWORD","Sensible123!")
DATABASE = os.environ.get("DB","master")
PORT = int(os.environ.get("PORT", "1433"))

if (SERVER is None or USER is None or PASSWORD is None or DATABASE is None):
    raise ValueError("Missing one or more of the following env variables: SERVER, USER, PASSWORD, DATABASE")


def seconds_to_days(seconds: int) -> float:
    return seconds / 60. / 60. / 24.


def main():
    start = datetime.utcnow()
    sleep_delay = SLEEP
    day_range = seconds_to_days(sleep_delay)  # look `sleep_delay` amount of time ahead if not backfill
    backfill_mode = BACKFILL

    if (backfill_mode):
        start = datetime(2017, 1, 1)
        day_range = MAX_DAY_RANGE
        sleep_delay = 1  # don't wait for new data if looking way in the past

    end = start + timedelta(days=day_range)

    while True:
        with pymssql.connect(server=SERVER, user=USER, password=PASSWORD, database=DATABASE, port=PORT) as conn:
            repo = DatumRepository(conn)
            for param, thresholds in PARAMS.items():
                print(f"Invalidating data for param ID {param} from {start} to {end} UTC")

                try:
                    repo.invalidate_events(param, start, end, thresholds)
                except Exception:
                    print(f"Error occurred invalidating param ID {param}:\n{traceback.format_exc()}")
                finally:
                    continue

        # slide start/end window
        start = end

        # once backfill catches up to now, update sleep delay and day range
        if (start >= datetime.utcnow() and backfill_mode is True):
            start = datetime.utcnow()  # reset start to now
            sleep_delay = SLEEP
            day_range = seconds_to_days(sleep_delay)
            backfill_mode = False

        end = start + timedelta(days=day_range)
        sleep(sleep_delay)


if __name__ == "__main__":
    main()
