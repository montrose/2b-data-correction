import pymssql
from datetime import datetime

conn = pymssql.connect(server='127.0.0.1', user='SA', password='Sensible123!', database='Master', port=1433)
cursor = conn.cursor()

#startDate = datetime(2021,10,1,19.1,4).strftime('%Y-%m-%d %H:%M:%S')
#endDate = datetime(2021,11,15,19.1,4).strftime('%Y-%m-%d %H:%M:%S')

cursor.execute("select DataId, ParameterId, PostDate, NumericValue from dbo.Datum where ParameterId > 100 and PostDate > N'2021-10-01 19:01:04.0000000' and Postdate <=  N'2021-12-28 21:01:05.0000000' order by ParameterId asc, PostDate asc")
data = cursor.fetchall()

time_threshold = 1800 # In seconds; test Value to start = 30Minutes
totalSpan = 120   # How many seconds to consider after an event starts
stablePeriod = 60 # After this period, a return to baseline ends an event
lowBaseline =  12 # Below this value means the instrument is not spiking any further
highBaseline = 24 # A value over this in an event means the event is suspect and should be invalidated

event_ids = []

for i in range(len(data[1:])):
    prev = data[i - 1]
    curr = data[i]
    previousSec = prev[2]
    currentSec = curr[2]
    seconds_diff = (currentSec - previousSec).total_seconds()
    if (seconds_diff >= time_threshold and curr[1] == prev[1]):  # Seconds more than threhold AND paramIds Match
        event_ids.append(i)

for idx in event_ids:
    reads = []  # A running log of all the rows in an event
    start = data[idx]
    reads.append(start) 

    eventMax = 0  # to Record the highest value in a given event
    k = 1
    
    #Things to keep Track of for 2 other tables
    startPostDate = start[2] #Initializing, does this even work?
    numOfPoints = 0                     #How many points are in the event / redundant with K so just use that, maybe subtract 1.
    
    while True:

        
        nextRow = data[idx + k]
        time_diff = (nextRow[2] - start[2]).total_seconds()

        if (time_diff >= totalSpan or nextRow[1] != start[1]):  # Stop if we exceed the search-time or if the parameter changes
            break
        if (time_diff >= stablePeriod and nextRow[3] < lowBaseline):  # Stop if after a set amount of time readings are under a certain amount
            break
        
        endPostDate = nextRow[2]
        reads.append(nextRow)
        k += 1

    for d in reads:
        reading = float(d[-1])
        if reading > eventMax:
            eventMax = reading
            # We only want to invalidate if there was a time where values were above xxx now!
            

    if eventMax >= highBaseline:
        
        #Query for Instrument and OrgID; set them here
        cursor.execute(("""SELECT I.InstrumentId, S.OrganizationId
                        FROM dbo.Parameters P 
                        join dbo.Instruments I on p.InstrumentId = I.InstrumentId
                        join dbo.sites S on S.siteId = I.SiteId
                        where P.ParameterId = %d"""), d[1])
        quickQuery = cursor.fetchone()
        instrumentId = quickQuery[0]
        organizationId = quickQuery[1]                     
            
        for point in reads:
            cursor.execute("UPDATE dbo.Datum set Invalidate =  N'1' where DataId = %d", int(point[0]))
        
        longQuery = f"""INSERT into dbo.DataTag values(
                        {instrumentId},
                        {point[1]},
                        N'{startPostDate.strftime('%Y-%m-%d %H:%M:%S')}',
                        N'{endPostDate.strftime('%Y-%m-%d %H:%M:%S')}',
                        {k},
                        'Spike Detection',
                        N'1',
                        'via Script',
                        N'{datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')}',
                        0,
                        {organizationId})
                       """
        cursor.execute(longQuery)
        conn.commit()
        # invalidate all of the rows in reading
    