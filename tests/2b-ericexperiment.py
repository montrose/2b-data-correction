from time import time
import pymssql
from datetime import datetime

conn = pymssql.connect(server='localhost', user='SA', password='Sensible123!', database='Master', port=14433)
cursor = conn.cursor()
parameterId = 693
#startDate = datetime(2021,10,1,19.1,4).strftime('%Y-%m-%d %H:%M:%S')
#endDate = datetime(2021,11,15,19.1,4).strftime('%Y-%m-%d %H:%M:%S')
startDate = datetime.datetime.now
endDate = datetime.datetime.now
cursor.execute("select DataId, ParameterId, PostDate, NumericValue from dbo.Datum where ParameterId = %s and PostDate > %s and Postdate <= %s order by ParameterId asc, PostDate asc", (parameterId, startDate, endDate))
   # where ParameterId > 100 and PostDate > N'2020-11-15 20:18:05.0000000'
   # order by ParameterId asc, PostDate asc""")
data = cursor.fetchall()

time_threshold = 1800 #In seconds; test Value to start = 30Minutes
SPAN = 60   #How many seconds to consider after an event

event_ids = []

# that should do it
for i in range(len(data[1:])):
    prev = data[i - 1]
    curr = data[i]
    previousSec = prev[2]
    currentSec = curr[2]
    seconds_diff = (currentSec - previousSec).total_seconds()
    if (seconds_diff >= time_threshold and curr[1] == prev[1]): #Seconds more than threhold AND paramIds Match
        # print(f"Time diff: {seconds_diff} | {i} | ({prev[1]}) {prev[2]} {prev[3]} | ({curr[1]}) {curr[2]} {curr[3]}")
        event_ids.append(i)

print("size:", len(event_ids))

invalids = {}  # dictionary(key = event ID, value = SPAN of following reads)

for idx in event_ids:
    reads = []
    start = data[idx]
    print(start)
    reads.append(start)
    
    k = 1
    while True:
        nextRow = data[idx + k]
        time_diff = (nextRow[2] - start[2]).total_seconds()

        if (time_diff >= SPAN or nextRow[1] != start[1]):
            break

        reads.append(nextRow)
        k += 1

    invalids[idx] = reads
    print([float(d[-1]) for d in reads])
    print("--------------")
