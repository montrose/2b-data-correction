from time import time
import pymssql
from datetime import datetime

conn = pymssql.connect(server='localhost', user='SA', password='Sensible123!', database='Master', port=14433)
cursor = conn.cursor()

cursor.execute("select DataId, ParameterId, PostDate, NumericValue from dbo.Datum where ParameterId > 100 and PostDate > N'2021-10-01 19:01:04.0000000' and Postdate <=  N'2021-12-28 21:01:05.0000000' order by ParameterId asc, PostDate asc")
   # where ParameterId > 100 and PostDate > N'2020-11-15 20:18:05.0000000'
   # order by ParameterId asc, PostDate asc""")
data = cursor.fetchall()

time_threshold = 1800 #In seconds; test Value to start = 30Minutes
SPAN = 60   #How many seconds to consider after an event

event_ids = []

# that should do it
for i in range(len(data[1:])):
    prev = data[i - 1]
    curr = data[i]
    previousSec = prev[2]
    currentSec = curr[2]
    seconds_diff = (currentSec - previousSec).total_seconds()
    if (seconds_diff >= time_threshold and curr[1] == prev[1]): #Seconds more than threhold AND paramIds Match
        # print(f"Time diff: {seconds_diff} | {i} | ({prev[1]}) {prev[2]} {prev[3]} | ({curr[1]}) {curr[2]} {curr[3]}")
        event_ids.append(i)

print("size:", len(event_ids))

invalids = {}  # dictionary(key = event ID, value = SPAN of following reads)

for idx in event_ids:
    reads = []
    start = data[idx]
    print(start)
    reads.append(start)
    
    k = 1
    while True:
        nextRow = data[idx + k]
        time_diff = (nextRow[2] - start[2]).total_seconds()

        if (time_diff >= SPAN or nextRow[1] != start[1]):
            break

        reads.append(nextRow)
        k += 1

    invalids[idx] = reads
    print([float(d[-1]) for d in reads])
    print("--------------")
